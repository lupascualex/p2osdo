# p2osdo



## Open source building blocks and managed infrastructure for preserving privacy during online search and discovery operations.

P2OSDO, symbolizing "Privacy preserving online search and discovery operations" represents a transformative research endeavor. Drawing from the H2020-funded PANORAMIX project, P2OSDO refines and tailors this advanced technology for specific online search and discovery use cases. Our mission is to elevate existing technologies, seamlessly integrating them into pioneering open-source modules. These modules not only ensure inherent privacy preservation but also foster a distributed trust ecosystem. Central to our vision, these modules will lay the foundation for a robust managed infrastructure, which is integral to our innovative business model. By leveraging mix-nets, layered encryption, differential privacy, homomorphic encryption, secure multi-party computation, and more, we cater to diverse scenarios, from querying databases and web-based searches to specialized protocols like RPC calls.

